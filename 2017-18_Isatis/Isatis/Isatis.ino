/*
   Main file of the final on-board software for CanSat ISATIS project.
   CSPU, 2017-18
*/

#include "IsatisConfig.h"
#include "IsatisAcquisitionProcess.h"

IsatisAcquisitionProcess process;

void setup() {
#ifdef INIT_SERIAL
  DINIT(serialBaudRate);
#endif
  process.init();
  DPRINTSLN(DBG_SETUP, "Setup complete");
}

void loop() {
  DPRINTSLN(DBG_LOOP, "*** Entering processing loop");
  process.run();
  DDELAY(DBG_LOOP, 500); // Slow main loop down for debugging.
  DPRINTSLN(DBG_LOOP, "*** Exiting processing loop");
}

