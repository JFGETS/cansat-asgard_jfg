/*
   RF_RealisticTest_Can

   A sketch which simulates the can side of the RF transmission:
     - By default permanently emit IsaTwo records every <period> msec, and listen
       for a string CMD_STRING. Timestamp is increased by 1 in the record
       each time, in order to be able to detect lost records.
     - When the string is received, answer with RSP_STRING, suspend emission
       for <suspensionDelay> seconds and during this delay, echo every string received on
       the RF channel.

     - The board waits for an actual USB Serial link, unless the SerialActivationPin is pulled down.

     Open issue: do we need a checksum to discard erroneous strings, or is
     it internal performed by the Xbee ?

*/

#include "DebugCSPU.h"
#include "IsaTwoRecord.h"
#include "elapsedMillis.h"
#include "string.h"

// Configuration constants
constexpr unsigned long emissionPeriod = 100; // msec.
constexpr unsigned long suspensionDelay = 10000; // msec
constexpr byte RF_RxPinOnUno = 9;
constexpr byte RF_TxPinOnUno = 11;
constexpr byte SerialActivationPin = 6; // If this pin is pulled-down the board will wait for Serial port initialisation,
// If it is open or pulled-up, it will not
//define SIMULATE_ON_USB_SERIAL // Define to have RF reception and transmission performed on Serial (for debugging)
//define PRINT_IGNORED_STRING   // Define to output the strings received from RF and discarded.
#define CMD_STRING "5,12345,67890"
#define RSP_STRING "**Command received**"
constexpr byte BufferSize = 150;
constexpr unsigned long RF_BaudRate = 115200; // This must be consistent with the configuration of the RF modules. 9600 is just enough!
constexpr bool showBufferOverlow = false; // Set to true to get messages on Serial when the reception buffer overflows.
constexpr bool timeRecordEmission = false; // Set to true to print on Serial the duration of the emission of a record.

// -----------------------------------------------------------------------------------------
//Define a RF Serial port.
#ifdef SIMULATE_ON_USB_SERIAL
auto &RF = Serial;
#else
#  ifdef ARDUINO_SAMD_FEATHER_M0_EXPRESS
HardwareSerial &RF = Serial1;
#   else
#   include "SoftwareSerial.h"
SoftwareSerial RF(RF_RxPinOnUno, RF_TxPinOnUno);
#   endif
#endif

IsaTwoRecord record;
bool acquisitionMode; // true if emitting records, false if answering commands.
elapsedMillis elapsed, heartbeatElapsed;

char buffer[BufferSize]; // The reception buffer.
byte bufferIndex; // index of first free position in buffer.

// Do not use more than 7 digits for the floats.
#define randomFloat1 123.4567
#define randomFloat2 234.5678
#define randomFloat3 345.6789
#define randomFloat4 456.7891
#define randomFloat5 567.8912
#define randomFloat6 678.9123
#define randomInt1 12345
#define randomInt2 23456
#define randomInt3 34567
#define randomInt4 45678
#define randomInt5 56789
#define randomInt6 67891

// Initialize content of record to be sent (except timestamp)
void initRecord(IsaTwoRecord &rec) {
  // Put some realistic data in the record to make sure it has a realistic length.
  rec.clear();
  rec.GPS_Altitude = randomFloat1;
  rec.GPS_LatitudeDegrees = randomFloat2;
  rec.GPS_LongitudeDegrees = randomFloat3;
  rec.accelRaw[0] = randomInt1;
  rec.accelRaw[1] = randomInt2;
  rec.accelRaw[2] = randomInt3;
  rec.gyroRaw[0] = randomInt4;
  rec.gyroRaw[1] = randomInt5;
  rec.gyroRaw[2] = randomInt6;
  rec.mag[0] = randomFloat4;
  rec.mag[1] = randomFloat5;
  rec.mag[2] = randomFloat6;
  rec.altitude = randomFloat1;
  rec.co2 = randomFloat2;
  rec.newGPS_Measures = true;
  rec.pressure = randomFloat3;
  rec.temperatureBMP=rec.temperatureThermistor = randomFloat4;
}

void processBuffer() {
  //Serial << ENDL << "Received '" << buffer << "'" << ENDL;
  if (strncmp(buffer, CMD_STRING, strlen(CMD_STRING)) == 0) {
    Serial << "Sending response" << ENDL;
    RF << RSP_STRING << ENDL;
    acquisitionMode = false;
    elapsed = 0;
  } else {
    if (acquisitionMode) {
#ifdef PRINT_IGNORED_STRINGS
      Serial << F("Ignoring unexpected string '") << buffer << "'" << ENDL;
#endif
    } else {
      Serial << F("Echoing received string... ") << ENDL;
      RF << buffer << ENDL;
    }
  }
  bufferIndex = 0; // Reset buffer pointer: content is processed.
}

void setup() {
  DINIT_IF_PIN(115200, SerialActivationPin);
  Serial << F("=== Simulating the CAN ===") << ENDL;
#ifdef SIMULATE_ON_USB_SERIAL
  Serial << F(" === Using USB Serial instead of RF Serial for test! ===") << ENDL;
#else
  RF.begin(RF_BaudRate);
#endif

  pinMode(LED_BUILTIN, OUTPUT);
  initRecord(record);
  acquisitionMode = true;
  elapsed = heartbeatElapsed = 0;
  int expectedStringLength = strlen(CMD_STRING);
  Serial << F("Receiver's ReferenceCSV constant should be set to the following string:") << ENDL;
  record.printCSV(Serial);
  Serial << ENDL;
#ifdef ARDUINO_SAMD_FEATHER_M0_EXPRESS
  Serial << "Feather board detected: using RX-TX pins for RF communication" << ENDL;
#else
  Serial << "Assuming AVR board: using softwareSerial (rx=" << RF_RxPinOnUno << ", tx=" << RF_TxPinOnUno
         << ") for RF communication" << ENDL;
#endif
  Serial << F("RF baud rate   : ") << RF_BaudRate << F(" baud") << ENDL;
  Serial << F("Expecting command strings using ") << expectedStringLength << F(" characters") << ENDL;
  Serial << F("Reception buffer size= ") << BufferSize;
  if (expectedStringLength > (BufferSize - 10)) {
    Serial << F(": *** add some buffer margin! ***") << ENDL;
  }
  else {
    Serial << F(": should be OK.") << ENDL;

  }
  Serial << F("IsaTwoRecord emission period=") << emissionPeriod << F(" msec") << ENDL;
  Serial << F("Suspension when received cmd=") << suspensionDelay / 1000.0 << F(" sec") << ENDL;
  Serial << ENDL << F("Setup complete") << ENDL;
}

void loop() {
  static int counter = 0;
  // Sending always performed with a complete record.
  if (acquisitionMode) {
    if (elapsed >= emissionPeriod)
    {
      elapsed = 0;
      counter++;
      if (timeRecordEmission) {
        unsigned long start = millis();
        record.printCSV(RF);
        unsigned long stop = millis();
        Serial << '(' << stop - start << " msec)";
      }
      else record.printCSV(RF);
      RF.println();
      record.timestamp++;
      Serial << ".";
      if (counter >= 100) {
        counter = 0;
        Serial << ENDL;
      }
    }
  }
  else {
    if (elapsed >= suspensionDelay) {
      acquisitionMode = true;
      elapsed = 0;
      counter = 0;
      Serial << F("Resuming acquisition mode") << ENDL;
    }
  }
  // Receiving always character by character, in order to
  // avoid delaying emission if garbage is received.
  while (RF.available()) {
    char c = RF.read();

    if (bufferIndex < (BufferSize - 1)) {
      if ((c == '\n') || (c == '\0')) {
        buffer[bufferIndex++] = '\0'; // end of string marker.
        processBuffer();
      } else {
        buffer[bufferIndex++] = c;
      }
    } else {
      if (showBufferOverlow) {
        Serial << "Error: buffer overflow" << ENDL;
      }
      bufferIndex = 0;
    }
  } // while RF.available.
  if (heartbeatElapsed > 500) {
    digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
    heartbeatElapsed = 0;
  }
}
