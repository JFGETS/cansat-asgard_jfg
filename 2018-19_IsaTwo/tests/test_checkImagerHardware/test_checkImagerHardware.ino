/*
   A utility sketch to check the hardware connected to the
   imager Feather board is present and responding.

   Also used to determine the required delay after turning regulator on.

   Synchronization with master clock NOT performed (requires the complete state machine).

   USB connexion is required.
*/


constexpr uint32_t cameraSPI_Speed = 4000000;
constexpr byte WaitUSB_Pin = 10;
constexpr unsigned long DelayAfterRegulatorOn = 0; // msec. delay after switching regulator on.
constexpr bool PowerDownBetweenCycles = true;
constexpr bool UseHW_Scanner = true; // if true, an HW_Scanner instance is used to
constexpr unsigned long DelayBetweenCycles = 2000; // msec
constexpr unsigned int NumSPI_Retries = 5; // Min = 1

// initialise the I2C bus and print complete diagnostic.
#define DEBUG
#include "DebugCSPU.h"
#include "IsaTwoConfig.h"
#include "SdFat.h"
#include "ArduCAM.h"
#include "Wire.h"
#include "ClockSynchronizer.h"
#include "HardwareScanner.h"
#define DBG_BEGIN 1
#define DBG_DIAGNOSTIC 1

SdFat mySD;
ArduCAM myCAM(OV2640, ImagerCamera_ChipSelect); // AYO 20190331

unsigned int counter = 0;

SPISettings spiCamSettings(cameraSPI_Speed, MSBFIRST, SPI_MODE0);

void enableRegulator(bool newStatus) {
  digitalWrite(ImageRegulatorEnablePin, (newStatus ? HIGH : LOW));
  Serial << "Regulator is now " << (newStatus ? "ON" : "OFF") << ENDL;
  if (newStatus) {
    Serial << "Waiting for camera and SD startup (" << DelayAfterRegulatorOn << " msec)..." ;
    delay(DelayAfterRegulatorOn);
    Serial << "OK" << ENDL;
  }
}

bool checkCameraSPI() {
  bool result = false;
  uint8_t temp;

  //Check if the ArduCAM SPI bus is OK
  SPI.beginTransaction(spiCamSettings);
  myCAM.CS_LOW();
  /*
    //Reset the CPLD AYO 20190331
    myCAM.write_reg(0x07, 0x80);
    delay(100);
    myCAM.write_reg(0x07, 0x00);
    delay(100);
  */

  myCAM.write_reg(ARDUCHIP_TEST1, 0x55);
  temp = myCAM.read_reg(ARDUCHIP_TEST1);
  myCAM.CS_HIGH();
  SPI.endTransaction();
  DPRINTS(DBG_BEGIN, "SPI: Wrote 0x55... Read again: 0x");
  DPRINTLN(DBG_BEGIN, temp, HEX);
  if (temp != 0x55) {
    DPRINTSLN(DBG_DIAGNOSTIC, "*** SPI interface Error! Did not read 0x55!");
  } else {
    DPRINTSLN(DBG_BEGIN, "SPI interface OK.");
    result = true;
  }
  return result;
}

bool checkCameraModel() {
  bool result=false;
#if defined (OV2640_MINI_2MP)
  DPRINTSLN(DBG_BEGIN, "Checking for OV2640...");
  uint8_t vid, pid;
  //Check if the camera module type is OV2640
  SPI.beginTransaction(spiCamSettings);
  myCAM.CS_LOW();
  myCAM.wrSensorReg8_8(0xff, 0x01);
  myCAM.rdSensorReg8_8(OV2640_CHIPID_HIGH, &vid);
  myCAM.rdSensorReg8_8(OV2640_CHIPID_LOW, &pid);
  myCAM.CS_HIGH();
  SPI.endTransaction();
  if ((vid != 0x26) && ((pid != 0x41) || (pid != 0x42))) {
    DPRINTSLN(DBG_DIAGNOSTIC, "Can't find OV2640 module! (retrying)");
  } else {
    DPRINTSLN(DBG_BEGIN, "OV2640 detected.");
    result=true;
  }
  return result;
#else
#error "Unsupported camera model"
#endif
}

void setup() {
  DINIT(115200);
  Serial << "============================================" << ENDL;
  Serial << "=      Imager hardware checking utility    =" << ENDL;
  Serial << "============================================" << ENDL << ENDL;

  pinMode(ImageRegulatorEnablePin, OUTPUT);
  enableRegulator(!PowerDownBetweenCycles);

  if (UseHW_Scanner) {
    HardwareScanner hw;
    hw.init();
    // TBC: can the wire initialization performed by the hw be overwritten by
    //      the one we need for clock syncho ?
    hw.printFullDiagnostic(Serial);
  }
  else {
    Serial << "HardwareScanner not used" << ENDL;
    Wire.begin();
    Serial << " TMP: I2C_SlaveControllerAddress = " <<  I2C_SlaveControllerAddress << ENDL;
  }

  pinMode(ImageRegulatorEnablePin, OUTPUT);
  if (PowerDownBetweenCycles) enableRegulator(false);

  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);
  delay(DelayBetweenCycles);
  digitalWrite(LED_BUILTIN, LOW);
  SPI.begin();
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);

  pinMode(ImagerSD_CardChipSelect, OUTPUT);
  digitalWrite(ImagerSD_CardChipSelect, HIGH); // AYO 20190331
  pinMode(ImagerCamera_ChipSelect, OUTPUT);
  digitalWrite(ImagerCamera_ChipSelect, HIGH);

}

void loop() {
  unsigned long masterClock;
  counter++;
  Serial << ENDL << counter << ": " ;
  enableRegulator(true);

  Serial << "Checking SD card" << ENDL;
  if (mySD.begin(ImagerSD_CardChipSelect)) {
    for (int i = 0; i < 3; i++) {
      digitalWrite(LED_BUILTIN, HIGH);
      delay(500);
      digitalWrite(LED_BUILTIN, LOW);
      delay(500);
    }
    digitalWrite(LED_BUILTIN, HIGH);
    Serial << "SD Card ok (CS="
           << ImagerSD_CardChipSelect << ")" << ENDL;
  } else {
    Serial << "*** SD.begin() failed. CS="
           << ImagerSD_CardChipSelect << " ***" << ENDL;
  }

  Serial << "Checking Camera SPI connexion" << ENDL;
  int i = 1;
  bool cameraPresent = false;
  while (!cameraPresent && i <= NumSPI_Retries) {
    Serial << "Attempt #" << i << "/" << NumSPI_Retries << ENDL;
    cameraPresent = checkCameraSPI();
    i++;
  }

  if (cameraPresent) {
    Serial << "Checking Camera model" << ENDL;
    i = 1;
    cameraPresent = false;
    while (!cameraPresent && i <= NumSPI_Retries) {
      Serial << "Attempt #" << i << "/" << NumSPI_Retries << ENDL;
      cameraPresent = checkCameraModel();
      i++;
    }
  }

  if (PowerDownBetweenCycles) enableRegulator(false);

  delay(3000);
}
