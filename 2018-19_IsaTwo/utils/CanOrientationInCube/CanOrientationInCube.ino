/* 
 *  Utility script to fine tune the can orientation in the cube. 
 */

constexpr bool useRadio=false;

#define DEBUG
#include "DebugCSPU.h"
#include "IsaTwoHW_Scanner.h"
#include "IMU_ClientLSM9DS0.h"
#include "IsaTwoConfig.h"

IsaTwoHW_Scanner hw;
IMU_ClientLSM9DS0 imu(1);
IsaTwoRecord rec;
HardwareSerial* rf;

void setup() {
  DINIT_IF_PIN(115200, DbgCtrl_MasterDigitalPinNbr);
  hw.IsaTwoInit();
  hw.printFullDiagnostic(Serial);
  if (!imu.begin()) {
	  Serial<< "Error initializing IMU" << ENDL;
	  while(1) { delay(100); } 
  }
  rf= hw.getRF_SerialObject();
  if (rf == NULL) {
    Serial << "Error: RF is null"<< ENDL;
    while(1) {
       delay(100);
    }
  }
  Serial << "Set-up OK" << ENDL;
  if (useRadio) {
    *rf << "Set-up OK" << ENDL << ENDL;
  }
}

void loop() {
  rec.clear();
  imu.readData(rec);
  float magnitude=sqrt(rec.accelRaw[0]*rec.accelRaw[0]+rec.accelRaw[1]*rec.accelRaw[1]+rec.accelRaw[2]*rec.accelRaw[2]);
  Serial << "Accel X = " << rec.accelRaw[0] << ", Y = "<< rec.accelRaw[1] << ", Z = " << rec.accelRaw[2] << ". Magnitude=" << magnitude << ENDL;
  if (useRadio) {
    *rf << "Accel X = " << rec.accelRaw[0] << ", Y = "<< rec.accelRaw[1] << ", Z = " << rec.accelRaw[2] << ". Magnitude=" << magnitude << ENDL;
  }
  delay(300);
}
