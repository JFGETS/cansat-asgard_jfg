/*
   AssertCSPU.h
 */
 /** 
 *  @file
   @brief This is a replacement for the standard assert.h which avoids the
   memory consuming globals used when __ASSERT_USE_STDERR is defined.
   
   When symbol USE_ASSERTIONS is defined (before inclusion of this header file!), 
   only minimal contexte information is  maintained but still results in significant 
   overhead in program memory.
   When symbol ASSERT is not defined, assert() results in 0 memory overhead.

   Typical use, through DebugCSPU.h:
   @code
   #define USE_ASSERTION // Comment this line out for operational software
   #define DEBUG
   #include "DebugCSPU.h"
   #define DBG_xxxx 0
   #define DBG_yyyy 1
   @endcode

   @remarks
          preprocessor symbol __FILE_NAME__ provides the full path and symbol __BASE_FILE__ provides
          the file as passed to gcc, which happens to be the full path as well. As a consequence, every
          single assertion consumes a significant amount of program memory (can be about 100-150 bytes
          depending on the file path!).
          A detailed search concluded that gcc does not provide the base name of the file and
          there is no portable compile-time solution to strip the path or even to limit it to the last 
          20 characters.
*/

#pragma once
#undef __ASSERT_USE_STDERR // To avoid memory-consuming globals

/* Always compile this function: assertions could be active in some compilation units
   and not in others. The function must always be known, always be in the library, so
   it can be linked when needed .*/

#ifdef ARDUINO_SAMD_FEATHER_M0_EXPRESS
void failCSPU(const char* __file, int __lineno, const char* __sexp);
#else
#include <WString.h> // To have the definition of __FlashStringHelper
void failCSPU(const __FlashStringHelper* __file, int __lineno, const __FlashStringHelper* __sexp);
#endif

#if !defined(USE_ASSERTIONS)
/** @cond HIDE_FROM_DOXIGEN */
#    define assert(e) ((void)0);
/** @endcond */
/** @ingroup DebugCSPU
 *  @brief Check condition e is true. If not interrupt program loudly. 
 *  
 *  Use DASSERT just like the standard assert function. When USE_ASSERTION is defined and the condition
 *  is not fulfilled, a message will  be output to Serial and the program stopped. \n
 *  if USE_ASSERTIONS is undefined (as should be in the operational software), assertions are completely
 *  stripped off. 
 */
#    define DASSERT(e)   ((void)0);
#else /* USE_ASSERTIONS */
/** @endcond */

#  ifdef ARDUINO_SAMD_FEATHER_M0_EXPRESS
#    define assert(e)  ((e) ? (void)0 : \
                       failCSPU(__FILE__, __LINE__, #e));
#    define DASSERT(e) ((e) ? (void)0 : \
                       failCSPU(__FILE__, __LINE__, #e));
#  else
#    define assert(e)  ((e) ? (void)0 : \
                       failCSPU(F(__FILE__), __LINE__, F(#e)));
#    define DASSERT(e) ((e) ? (void)0 : \
                       failCSPU(F(__FILE__), __LINE__, F(#e)));
#  endif // else
// Definition at 0 memory cost. Useful ? I guess if it fails and we do not get any notification, we do
// not want to abort()...
// #define assert(e) ((e) ? (void)0 : abort());


#endif /* ASSERT */
/** @} */
