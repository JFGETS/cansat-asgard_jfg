/*
 * test_FeatherM0_Express
 * 
 * This sketch checks a number of features which are different on a Feather board
 * compared to a Genuino Uno.
 * 
 * To upload: Install both the Arduino SAMD and the Adafruit SAMD Package
 * 
 * µC is ATSAMD21G18 (SAMD architectrue, based on ARM Cortex-M0+ processor).
 * NB: SAM = "Smart ARM-based microcontroller", various flavours, including SAM-D
 * ARM = family of RISC architectures. 
 * 
 * The AVR is a modified Harvard architecture machine, where program and data are stored in 
 * separate physical memory systems that appear in different address spaces, but having the 
 * ability to read data items from program memory using special instructions. 
 * In short: AVR has separate memories & buses for program and data
 * 
 * The 32bit AVR and 8bit AVR architectures are actually completely different from each other.  
 * And Arduino doesn't run on 32bit AVRs.  So if you want to compare arduinos with AVRs (Uno, Nano, 
 * Leonardo) and Arduinos with ARMs (Due, Zero, Teensy), the big difference IS that the AVR is 
 * an 8-bit architecture, and the ARM is a 32 bit architecture.
 * From a practical point of view, ARMs tend to have significantly more memory, run at higher 
 * clock rates, and have more complex peripherals.  
 * AVRs run over a wider voltage range and have higher current drive.
 * 
 * About string storage:
 *   - With Genuino UNO, it is very clear: F() macro is required.
 *   - With SAMD: F() does not make any difference. Everything is in program memory
 *     automatically whenever possible. 
 */

#define DEBUG
#include "DebugCSPU.h"
#include "FeatherM0.h"
#define DBG 1

// Define this to have long strings stored in program memory. 
#define LONG_STRINGS_TO_PGRM_MEMORY
#define LONG_STRINGS_TO_DYNAMIC_MEMORY

#ifdef SAMD_FEATHER_M0_EXPRESS
#include "Serial2.h"
#endif

void setup() {
  DINIT(115200);
  // Usage of Serial
  Serial << "USB Serial port OK." << ENDL;

  // Detection of board and architecture with #ifdefs
#ifdef ARDUINO_ARCH_SAM
  Serial << "ARDUINO_ARCH_SAM architecture detected" << ENDL;
#else
  Serial << "ARDUINO_ARCH_SAM architecture NOT detected." << ENDL;
#endif

#ifdef ARDUINO_ARCH_SAMD
  Serial << "ARDUINO_ARCH_SAMD architecture detected" << ENDL;
#else
  Serial << "ARDUINO_ARCH_SAMD architecture NOT detected" << ENDL;
#endif

#ifdef ARDUINO_SAMD_ZERO
  Serial << "ARDUINO_SAMD_ZERO architecture detected" << ENDL;
#else
  Serial << "ARDUINO_SAMD_ZERO architecture NOT detected" << ENDL;
#endif

#ifdef ARDUINO_SAMD_FEATHER_M0_EXPRESS
  Serial << "ARDUINO_SAMD_FEATHER_M0_EXPRESS board detected" << ENDL;
#else
  Serial << "ARDUINO_SAMD_FEATHER_M0_EXPRESS board  NOT detected" << ENDL;
#endif

#ifdef ADAFRUIT_FEATHER_M0_EXPRESS
  Serial << "ADAFRUIT_FEATHER_M0_EXPRESS board detected" << ENDL;
#else
  Serial << "ADAFRUIT_FEATHER_M0_EXPRESS board  NOT detected" << ENDL;
#endif

#ifdef ARDUINO_SAMD_ZERO
  Serial << "ARDUINO_SAMD_ZERO is defined" << ENDL;
#else
  Serial << "ARDUINO_SAMD_ZERO is not defined" << ENDL;
#endif

  // Display of available RAM
  DFREE_RAM(DBG);
   
  // To be tested: 
  // Detection of additional serial ports

  // Memory alignment
  // Output of floating point variables
  // Storage of strings in program memory (does the F() still work ?)
#ifdef LONG_STRINGS_TO_PGRM_MEMORY
  Serial << "Storing a couple of long strings in program memory" << ENDL;
  Serial << "Write program size, and recompile without defining LONG_STRINGS_TO_PGRM_MEMORY" << ENDL;
  Serial << "Check program size and dynamic memory usage" << ENDL;
  const char str[]="01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567XX";
  const char* str2="01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567YY";
  Serial << F("0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789") << ENDL;
  Serial << F("012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678a") << ENDL;
  Serial << F("012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678b") << ENDL;
  Serial << str << ENDL;
  Serial << str2 <<  ENDL;
#endif
#ifdef LONG_STRINGS_TO_DYNAMIC_MEMORY
 Serial << "Storing a couple of long strings in dynamic memory" << ENDL;
  Serial << "Write program size, and recompile defining LONG_STRINGS_TO_PGRM_MEMORY" << ENDL;
  Serial << "Check program size and dynamic memory usage" << ENDL;
  // For SAMD, only the variable string is in dynamic memory, even without F() macro.
  const char str3[]="01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567XX";
  const char* str4="01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567YY";
  Serial << "0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789" << ENDL;
  Serial << "012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678a" << ENDL;
  Serial << "012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678b" << ENDL;
  Serial << str3 << ENDL;
  Serial << str4 <<  ENDL;
#endif

  // Access to Flash memory (including access to files through USB with helper programs).

  
  // RTC ??

  // Using a second Serial port
  Serial << "To test Serial1 and Serial2 ports, run test_Serial2-3 sketch (CansatAsgard Library)." << ENDL;
  
  Serial << ENDL << "End of job." << ENDL;
}

void loop() {
  // put your main code here, to run repeatedly:

}
