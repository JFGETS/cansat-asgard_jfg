#include "EEPROM_BankWithTools.h"

//#define USE_ASSERTIONS
#define DEBUG
#include "DebugCSPU.h"

EEPROM_BankWithTools::EEPROM_BankWithTools(
  const unsigned int theMaintenancePeriodInSec) : EEPROM_Bank(theMaintenancePeriodInSec) {

}


void EEPROM_BankWithTools::printHeaderFromMemory() const {
#ifdef USE_ASSERTIONS
  const HardwareScanner* hardware = getHardware();
#endif
  DASSERT(hardware);  // This makes sure the EEPROM_Bank is initialized.
  EEPROM_Header h = getHeader();
  char str[60];
  sprintf(str, "Header found in memory (%d bytes).", (unsigned int) sizeof(h));
  Serial.println(str);
  sprintf(str, "   Key            : 0x%04x", h.headerKey);
  Serial.println(str);
  Serial <<  F("   NumChips       : ") << (int)  h.numChips << ENDL;
  sprintf(str, "   Last address   : %u (0x%04x)" , h.chipLastAddress , h.chipLastAddress);
  Serial.println(str);
  sprintf(str, "   Record size    : %u (0x%04x) bytes.", h.recordSize, h.recordSize);
  Serial.println(str);
  Serial <<  F("   First free chip: ") << (int) h.firstFreeChip << ENDL;
  sprintf(str, "   First free byte: 0x%04x", h.firstFreeByte);
  Serial.println(str);
}

void EEPROM_BankWithTools::printHeaderFromChip() const {
#ifdef USE_ASSERTIONS
  const HardwareScanner* hardware = getHardware();
#endif
  DASSERT(hardware);  // This makes sure the EEPROM_Bank is initialized. 
  EEPROM_Header h;

  h.headerKey = getHeader().headerKey + 1;
  readFromEEPROM(0, 0, (byte *) &h, sizeof(EEPROM_Header));

  char str[60];
  sprintf(str, "Header found in chip #0 (%d bytes).", (unsigned int) sizeof(h));
  Serial.println(str);
  sprintf(str, "   Key            : 0x%04x", h.headerKey);
  Serial.println(str);
  Serial <<  F("   NumChips       : ") << (int)  h.numChips << ENDL;
  sprintf(str, "   Last address   : %u (0x%04x)" , h.chipLastAddress , h.chipLastAddress);
  Serial.println(str);
  sprintf(str, "   Record size    : %u (0x%04x) bytes.", h.recordSize, h.recordSize);
  Serial.println(str);
  Serial <<  F("   First free chip: ") << (int) h.firstFreeChip << ENDL;
  sprintf(str, "   First free byte: 0x%04x", h.firstFreeByte);
  Serial.println(str);
}
