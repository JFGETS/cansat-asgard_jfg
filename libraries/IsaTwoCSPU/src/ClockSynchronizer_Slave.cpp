/*
   ClockSynchronizer_Slave.cpp
*/
#include "ClockSynchronizer.h"
#include "Wire.h"

//#define DEBUG
#include "DebugCSPU.h"

bool ClockSynchronizer_Slave::syncReceived = false;
long ClockSynchronizer_Slave::masterAdvance = 0;

void ClockSynchronizer_Slave::begin() {
  Wire.onReceive (receiveSyncEvent);
  syncReceived=false;
}

// The IRQ handler called by interrupt service routine when incoming data arrives
void ClockSynchronizer_Slave::receiveSyncEvent(int howMany) {
  unsigned long currentTS=millis(); // take current time ASAP
  if (howMany == sizeof(masterAdvance)) {
    // Assume we are receiving a clock sync: read an unsigned long
    uint8_t *c = (uint8_t*) &masterAdvance;
    for (int i = 0; i < howMany; i++)
    {
      *c = Wire.read();
       c++;
    }  // end of for loop
    masterAdvance-=currentTS; // compute advance. 
    syncReceived = true;
  }
  else {
    // discard.
    for (int i = 0; i < howMany; i++)
    {
      Wire.read ();
    }
  }
}  // end of receiveSyncEvent

bool ClockSynchronizer_Slave::getMasterClock(unsigned long &masterClock) {
   masterClock=millis()+masterAdvance;
   return syncReceived;
}
