/*
    IsaTwoXBeeClient.cpp
*/

#include "IsaTwoXBeeClient.h"

#define DEBUG
#include "DebugCSPU.h"
#define DBG_DIAGNOSTIC 1
#define DBG_STREAM 0


bool IsaTwoXBeeClient::send(const IsaTwoRecord& record, int timeOutCheckResponse) {
  // Beware of byte alignment. Only even adresses can be adressed directly
  // in SAMD 16-bit architecture.
  uint8_t buffer[sizeof(IsaTwoRecord) + 2];
  buffer[0] = (uint8_t) IsaTwoRecordType::DataRecord;
  buffer[1] = 0;
  memcpy(buffer + 2, &record, sizeof(record));

#ifdef XBEECLIENT_INCLUDES_UTILITIES
#ifdef DBG_PRINT_OUTGOING_FRAMES
  if (displayOutgoingFramesOnSerial) {
	  displayFrame((uint8_t *)  buffer, (uint8_t) (sizeof(record) + 2));
  }
#endif
#endif
  return XBeeClient::send((uint8_t *)  buffer, (uint8_t) (sizeof(record) + 2), timeOutCheckResponse);
}

void IsaTwoXBeeClient::openStringMessage(IsaTwoRecordType recordType) {
  DPRINTS(DBG_STREAM, "IsaTwoXBeeClient::openStringMessage(), type= ");
  DPRINTLN(DBG_STREAM, (uint8_t) recordType);
  XBeeClient::openStringMessage( (uint8_t) recordType);
}

bool IsaTwoXBeeClient::getDataRecord(IsaTwoRecord& record, const uint8_t* data, uint8_t dataSize) {
  if (dataSize != (sizeof(IsaTwoRecord) + 2)) {
    DPRINTS(DBG_DIAGNOSTIC, "*** Error: inconsistent size. size=");
    DPRINT(DBG_DIAGNOSTIC, dataSize);
    DPRINTS(DBG_DIAGNOSTIC, ", IsaTwoRecord size=");
    DPRINTLN(DBG_DIAGNOSTIC, sizeof(IsaTwoRecord));
    return false;
  }
  if (data[0] != (uint8_t) IsaTwoRecordType::DataRecord) {
    DPRINTS(DBG_DIAGNOSTIC, "*** Error: inconsistent record type (");
    DPRINT(DBG_DIAGNOSTIC, data[0]);
    DPRINTS(DBG_DIAGNOSTIC, ", expected ");
    DPRINTLN(DBG_DIAGNOSTIC, (uint8_t) IsaTwoRecordType::DataRecord);
    return false;
  }
  memcpy(&record, data + 2, sizeof(record));
  return true;
}

bool IsaTwoXBeeClient::getString(char* str, const uint8_t* data, uint8_t dataSize) {
	bool result=false;

	if (dataSize <=2) {
		DPRINTS(DBG_DIAGNOSTIC, "*** Error: received empty string");
		*str ='\0';
		return result;
	}
	const char* s = (const char*) (data + 2);

	switch(data[0]) {
	case (uint8_t) IsaTwoRecordType::StatusMsg:
	case (uint8_t) IsaTwoRecordType::CmdRequest:
	case (uint8_t) IsaTwoRecordType::CmdResponse:
	case (uint8_t) IsaTwoRecordType::StringPart:
		if (dataSize != (uint8_t) strlen(s) + 3) {
			// 2 bytes + string + '\0'
			DPRINTS(DBG_DIAGNOSTIC, "*** Error: inconsistent size. size=");
			DPRINT(DBG_DIAGNOSTIC, dataSize);
			DPRINTS(DBG_DIAGNOSTIC, ", String size, from third byte=");
			DPRINTLN(DBG_DIAGNOSTIC, strlen(s));
		} else {
			memcpy(str, data+2, strlen(s)+1);
			result=true;
		}
		break;
	default:
		DPRINTS(DBG_DIAGNOSTIC, "*** Error: inconsistent record type (");
		DPRINT(DBG_DIAGNOSTIC, data[0]);
		DPRINTS(DBG_DIAGNOSTIC, ", expected StatusMsg, CmdRequest or CmdResponse");
		break;
	} // switch
	return result;
}

#ifdef XBEECLIENT_INCLUDES_UTILITIES

void IsaTwoXBeeClient::displayFrame(uint8_t* data, uint8_t dataSize)
{
  unsigned char type = data[0];
  IsaTwoRecord rec;
  const char* s;
  Serial << "--- Frame content ---" << ENDL;
  switch (type) {
    case (int) IsaTwoRecordType::DataRecord:
      Serial << "DataRecord message" << ENDL;
      if (getDataRecord(rec, data, dataSize)) {
        rec.print(Serial);
        Serial << ENDL << "  --- End of record ---" << ENDL;
      }
      break;
    case (int) IsaTwoRecordType::StatusMsg:
      s = (const char*) (data + 2);
      Serial << "Status message carrying string: '" << s << "'" << ENDL;
      Serial << "length=" << strlen(s) << ", size=" << dataSize;
      if (strlen(s) + 2 != (size_t) (dataSize)) {
        Serial << ENDL << "*** Error: inconsistent size" << ENDL;
      }
      if ((strlen(s) > 0) && (s[strlen(s) - 1] == '\n')) {
        Serial << "(last character is an ENDL)";
      }
      Serial << ENDL;
      break;
    default:
      Serial << "*** Unsupported type: " << type << ENDL;
  }
  Serial << "--- End of frame content ---" << ENDL;
}

bool IsaTwoXBeeClient::send(  uint8_t* data,
                              uint8_t dataSize,
                              int timeOutCheckResponse,
                              uint32_t SH, uint32_t SL) {
  DPRINTSLN(DBG_STREAM, "IsaTwoXBeeClient::send() ");
  if (displayOutgoingFramesOnSerial) {
    displayFrame(data, dataSize);
  }
  return XBeeClient::send(data, dataSize, timeOutCheckResponse, SH, SL);
};
#endif
