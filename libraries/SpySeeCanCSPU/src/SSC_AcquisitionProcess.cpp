/*
   SSC_AcquisitionProcess.cpp
*/
#define USE_TIMER

#include "SSC_AcquisitionProcess.h"
#include "SSC_Config.h"



#ifdef ARDUINO_SAMD_FEATHER_M0_EXPRESS
#include "Serial2.h"
HardwareSerial &gpsSerial = (GPS_SerialPortNumber == 1 ? Serial1 : Serial2);
#elif defined(__AVR__)
#include "SoftwareSerial.h"
SoftwareSerial gpsSerial(3 , 2);
#endif


SSC_AcquisitionProcess::SSC_AcquisitionProcess()
  : AcquisitionProcess(SSC_AcquisitionPeriod),
    hwScanner(unusedAnalogInPinNumber),
    gps(gpsSerial),
    storageMgr(SSC_CampainDuration, SSC_AcquisitionPeriod),
    averageSpeed(0.0f)
{
  campaignStarted = false;
  lastRecordAltitude = -1;
}

void SSC_AcquisitionProcess::init() {
  DPRINTSLN(DBG_INIT, "SSC_AcquisitionProcess::init");

  // Initialize scanner BEFORE calling AcquisitionProcess::init
  hwScanner.SSC_Init();
#ifdef PRINT_DIAGNOSTIC_AT_INIT
  hwScanner.printFullDiagnostic(Serial);
  DDELAY(DBG_INIT, 1000); //Why?
#endif

  AcquisitionProcess::init();
  String CSV_Header;
  CSV_Header.reserve(200);
  StringStream sstr(CSV_Header);
  record.printCSV_Header(sstr);
  storageMgr.init(&hwScanner, "SSC_", 4, SD_RequiredFreeMegs, EEPROM_KeyValue, CSV_Header);

  auto RF = GET_RF_STREAM(hwScanner);
  if (RF != NULL)
  {
    RF_OPEN_STRING(RF);
    *RF << F("\nLogging to ") << storageMgr.getSD_FileName() << ENDL;
    RF_CLOSE_STRING(RF);
  }

  //What are EEPROMS?
#if (defined(SSC_USE_EEPROMS) && (DBG_DIAGNOSTIC==1))
  float duration = ((float) storageMgr.getNumFreeEEEPROM_Records()) * SSC_AcquisitionPeriod / 1000.0 / 60.0;
  Serial << F("EEPROM can accept ") << duration << F(" min. of data. (key=0x");
  Serial.print(EEPROM_KeyValue, HEX);
  Serial << ")" << ENDL;
  if (RF != NULL)
  {
    RF_OPEN_STRING(RF);
    *RF << F("EEPROM can accept ") << duration << F(" min. of data. Key=0x");
    RF->println(EEPROM_KeyValue, HEX); // This is currently not supported in API mode. Method to be added.
    RF_CLOSE_STRING(RF);
  }
#endif

#ifdef PRINT_ACQUIRED_DATA_CSV
  {
    Serial << ENDL;
    record.printCSV_Header(Serial);
    Serial << ENDL;
  }
#endif

  if (MinSpeedToStartCampaign > 10.0f) {
    DPRINTSLN(DBG_DIAGNOSTIC, "*** Error: MinSpeedToStartCampaign should be lower than 10m/s");
  }

  DPRINTSLN(DBG_INIT, "SSC_AcquisitionProcess::init done");
}

SSC_HW_Scanner*  SSC_AcquisitionProcess::getHardwareScanner()
{
  return &hwScanner;
}

void SSC_AcquisitionProcess::startMeasurementCampaign(const char* msg, float value) {
  campaignStarted = true;
  //comment ça se fait qu'il ne faut pas mettre HardwareScanner::LED_Type::Campaign
  setLED(HardwareScanner::Campaign, AcqOff);

  // Do not start imager: it will be started during the next run
  // of AcquireDataRecord, since the campaignStarted will be true,
  // and the control line HIGH.
  String str("# === CAMPAIGN STARTED at ");
  // Initial # to create a comment line in the SD file.
  StringStream ss(str);
  ss << millis() << ": " << msg << " (" << value << ") ===";
  DPRINTLN(DBG_DIAGNOSTIC, str.c_str());
  auto RF = GET_RF_STREAM(hwScanner);
  if (RF != NULL) {
    RF_OPEN_STRING(RF);
    *RF << str.c_str() << ENDL;
    RF_CLOSE_STRING(RF);
  }
  storageMgr.storeString(str);
}

void SSC_AcquisitionProcess::stopMeasurementCampaign() {
  campaignStarted = false;
  lastRecordAltitude = -1;
  averageSpeed = 0.0f;
  setLED(HardwareScanner::Campaign, AcqOn);


  DPRINTSLN(DBG_DIAGNOSTIC, "");
  DPRINTSLN(DBG_DIAGNOSTIC, "=== CAMPAIGN STOPPED ===");
  auto RF = GET_RF_STREAM(hwScanner);
  if (RF != NULL) {
    RF_OPEN_STRING(RF);
    *RF << ENDL << F("=== CAMPAIGN STOPPED ===") << ENDL;
    RF_CLOSE_STRING(RF);
  }
}


bool SSC_AcquisitionProcess::measurementCampaignStarted() {
  static byte ignoredSamples = 10;
  static elapsedMillis elapsedSinceLastInfo = 0;
  static unsigned long lastRecordTimestamp = 0;

  if (campaignStarted == true ) {
    return true;
  }

  // Let's not start before readings are stable
  if (ignoredSamples > 0) {
    ignoredSamples--;
    lastRecordTimestamp = record.timestamp; // maintain the timestamp, otherwise it would cause an interruption and a new reset.
    return false;
  }

  // Check absolute altitude. This is a security, to cover for a µC reset during the flight
  if (record.altitude >= AltitudeThresholdToStartCampaign) {
    DPRINTS(DBG_DIAGNOSTIC, "Forcing campaign start because altitude > ");
    DPRINTLN(DBG_DIAGNOSTIC, AltitudeThresholdToStartCampaign);
    startMeasurementCampaign("Altitude above threshold!", record.altitude);
    return true;
  }

  // Acquisition mode could have been interrupted, and our last information
  // could be outdated. In this case, we start averaging again
  if ((record.timestamp - lastRecordTimestamp) > (3 * SSC_AcquisitionPeriod)) {
    DPRINTSLN(DBG_DIAGNOSTIC, "Reseting average speed calculation");
    lastRecordTimestamp = record.timestamp;
    lastRecordAltitude = -1; // This will cause a reset of the averaging.
    ignoredSamples = 10;   // This will cause the next 10 samples to be ignored.
    return false;
  }

  if (lastRecordAltitude == -1) {
    averageSpeed = 0.0f; // Reset here in case the campaign was stopped manually after startup
    lastRecordAltitude = record.altitude;
    lastRecordTimestamp = record.timestamp;

    DPRINTSLN(DBG_DIAGNOSTIC, "");
    DPRINTS(DBG_DIAGNOSTIC, " ...Waiting for average speed of ");
    DPRINT(DBG_DIAGNOSTIC, MinSpeedToStartCampaign);
    DPRINTS(DBG_DIAGNOSTIC, " m/s during ");
    DPRINT(DBG_DIAGNOSTIC, NumSamplesToAverageForStartingCampaign);
    DPRINTS(DBG_DIAGNOSTIC, " readings. last rec. altitude=");
    DPRINTLN(DBG_DIAGNOSTIC, lastRecordAltitude);
    return false;
  }



  float speed = (record.altitude - lastRecordAltitude) / (record.timestamp - lastRecordTimestamp) * 1000;
  // Limit speed to 10 m/s to avoid than a single aberrant reading would increase
  // the average beyond the threshold.
  if (speed > 10.0f) speed = 10.0f;
  averageSpeed -= averageSpeed / NumSamplesToAverageForStartingCampaign;
  averageSpeed += speed / NumSamplesToAverageForStartingCampaign;

#ifdef DBG_CAMPAIGN_STARTED
  if ((elapsedSinceLastInfo > ReportingPeriodWhileWaitingForStartCampaign) &&
      (averageSpeed > 2.0 * MinSpeedToStartCampaign / 3.0) ) {
    auto RF = GET_RF_STREAM(hwScanner);
    if (RF != NULL)
    {
      RF_OPEN_STRING(RF);
      *RF << millis() << ": Waiting for take-off..." << ENDL;
      RF_CLOSE_STRING(RF);
      RF_OPEN_STRING(RF);
      *RF <<  "   current  alt.= " << record.altitude << ", prev. alt.= " << lastRecordAltitude << ENDL;
      RF_CLOSE_STRING(RF);
      RF_OPEN_STRING(RF);
      *RF <<  "   current speed= " << speed << ", avg speed= " << averageSpeed << ENDL;
      RF_CLOSE_STRING(RF);
    }
    DPRINTS (DBG_CAMPAIGN_STARTED, "Waiting for take-off.  curr. alt.=");
    DPRINT(DBG_CAMPAIGN_STARTED, record.altitude);
    DPRINTS (DBG_CAMPAIGN_STARTED, "  prev. altitude=");
    DPRINT(DBG_CAMPAIGN_STARTED, lastRecordAltitude);
    DPRINTS (DBG_CAMPAIGN_STARTED, "  speed=");
    DPRINT(DBG_CAMPAIGN_STARTED, speed);
    DPRINTS (DBG_CAMPAIGN_STARTED, "  avg speed=");
    DPRINTLN(DBG_CAMPAIGN_STARTED, averageSpeed);
    elapsedSinceLastInfo = 0;
  }
#endif
  lastRecordAltitude = record.altitude;
  lastRecordTimestamp = record.timestamp;

  if (averageSpeed > MinSpeedToStartCampaign) {
    startMeasurementCampaign("Average speed above threshold!", averageSpeed);
    return true;
  } else {
    return false;
  }
}

void SSC_AcquisitionProcess::acquireDataRecord() {
  static byte recordCounter = 0;
  DBG_TIMER("SSC_AcquisitionProcess::acquireDataRecord");
  DPRINTSLN(DBG_ACQUIRE, "SSC_AcquisitionProcess::acquireDataRecord()");
  record.clear();
  record.timestamp = millis();

  DPRINTSLN(DBG_ACQUIRE, "Reading BMP");
  bmp.readData(record);
  DPRINTSLN(DBG_ACQUIRE, "Reading GPS");
  gps.readData(record);
  DPRINTSLN(DBG_ACQUIRE, "Reading thermistors");
  thermistors.readData(record);

  //add other clients...


#ifdef PRINT_ACQUIRED_DATA
  {
    DBG_TIMER("Serial output");
    Serial << ENDL;
    record.print(Serial);
  }
#endif

#ifdef PRINT_ACQUIRED_DATA_CSV
  //why this condition?
  if (campaignStarted || (recordCounter >= 50))
  {
    DBG_TIMER("Serial output CSV");
    record.printCSV(Serial);
    Serial.println();
    if (!campaignStarted) {
      Serial.println( record.newGPS_Measures ? "GPS Fix OK" : "No GPS fix.");
    }
  }
#endif
  DDELAY(DBG_SLOW_DOWN_TRANSMISSION, 500);

  // send on RF
  auto RF = GET_RF_STREAM(hwScanner);
  if (RF != NULL)
  {
    DBG_TIMER("RF transmission");
    if (campaignStarted || (recordCounter >= 50)) {
      setLED(HardwareScanner::Transmission, AcqOn);
#ifdef RF_ACTIVATE_API_MODE
      RF->send(record);
#else
      record.printCSV(*RF); // Print slowly to avoid buffer overflow.
      RF->println();
#endif
      recordCounter = 0;
      if (!campaignStarted) {
        RF_OPEN_STRING(RF);
        *RF << ( record.newGPS_Measures ? "GPS Fix OK" : "No GPS fix.") << ENDL;
        RF_CLOSE_STRING(RF);
        String CSV_Buffer;
        StringStream str(CSV_Buffer);
        record.printCSV(str);
        RF_OPEN_STRING_PART(RF);
        *RF << CSV_Buffer.substring(0, 80) << ENDL;
        RF_CLOSE_STRING_PART(RF);
        RF_OPEN_STRING(RF);
        *RF << CSV_Buffer.substring(81) << ENDL;
        RF_CLOSE_STRING(RF);
        DPRINTLN(DBG_DIAGNOSTIC, CSV_Buffer.c_str());
      }
      setLED(HardwareScanner::Transmission, AcqOff);
    }
  } // RF not null
  recordCounter++;  // Increase even if not printed or sent on RF.
  //Again, not sure what this does
  /*
    if (campaignStarted) {
    if (digitalRead(ImagerCtrl_MasterDigitalPinNbr)==HIGH) {
      // campaign started and Imager is not active: turn it on.
      DPRINTSLN(DBG_SLAVE_SYNC,"Starting imager");
      startImager();
    }
    } // campaign started.
    DPRINTSLN(DBG_ACQUIRE, "End acquireDataRecord()");
  */
}

void SSC_AcquisitionProcess::storeDataRecord(const bool campaignStarted) {
  if (campaignStarted) {
#ifdef SSC_USE_EEPROMS
    setLED(HardwareScanner::UsingEEPROM, AcqOn); // The storage LED is managed by IsaTwoAcquisitionProcess::run().
    storageMgr.storeSSC_Record(record, true); // during campaign, store in EEPROM
    setLED(HardwareScanner::UsingEEPROM  , AcqOff);
#else
    storageMgr.storeSSC_Record(record, false);
#endif
  } else {
    storageMgr.storeSSC_Record(record, false); // Out of campaign do not store in EEPROM
  }
}

void SSC_AcquisitionProcess::initSpecificProject() {
  DPRINTSLN(DBG_INIT, "SSC_AcquisitionProcess::initSpecificProject");
  bool result;

  /*if (hwScanner.isI2C_SlaveUpAndRunning(I2C_BMP_SensorAddress)) {
     result = bmp.begin(SeaLevelPressure_HPa);
     if (!result) {
       DPRINTSLN(DBG_DIAGNOSTIC, "Error initializing BMP");
     }
    }*/

  result = bmp.begin(SeaLevelPressure_HPa);
  if (!result) {
    DPRINTSLN(DBG_DIAGNOSTIC, "Error initializing BMP");
  }

  gps.begin((SSC_GPS_Client::Frequency) SSC_GPS_Frequency);

  //what is this used for?
  auto RF = GET_RF_STREAM(hwScanner);
  // Send diagnostic and header to RF
  if (RF != NULL)
  {
    setLED(HardwareScanner::Transmission, AcqOn);
    // This is ugly and highly inefficient: to be redesigned...
    String stringBuffer;
    StringStream s(stringBuffer);
    hwScanner.printFullDiagnostic(s);
    auto last = stringBuffer.length() - 1;
    unsigned int first = 0;
    while (first <= last) {

      if (first + 80 < last) {
        RF_OPEN_STRING_PART(RF);
        *RF << stringBuffer.substring(first, first + 80);
        first += 80;
        RF_CLOSE_STRING_PART(RF);
      } else {
        RF_OPEN_STRING(RF);
        *RF << stringBuffer.substring(first, last).c_str();
        first = last + 1;
        RF_CLOSE_STRING(RF);
      }

      delay(100);
    }
    setLED(HardwareScanner::Transmission, AcqOff);
  }
  //Again, what?
  //digitalWrite(ImagerCtrl_MasterDigitalPinNbr, HIGH); // Do not start imager.
  record.clear();

  DPRINTS(DBG_DIAGNOSTIC, "SSC_SensorsWarmupDelay...");
  delay(SSC_SensorsWarmupDelay);
  DPRINTSLN(DBG_DIAGNOSTIC, "Done.");

  DPRINTSLN(DBG_INIT, "End of SSC_AccquisitionProcess::initSpecificProject");
}

void SSC_AcquisitionProcess::doIdle() {
  storageMgr.doIdle();
}
