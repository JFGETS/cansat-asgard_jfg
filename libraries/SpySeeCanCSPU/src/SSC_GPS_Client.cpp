#include <Arduino.h>
#include "SSC_GPS_Client.h" //includes the .h 
#include "TC_Timer.h"

#define DEBUG
#include "DebugCSPU.h"
#define DBG 1
#define DBG_NMEA 0
#define DBG_DIAGNOSTIC 1
#define DBG_FREQUENCY 1
#define DBG_TIMER 1

#define GPS_CLIENT_TIMER 4 // valid values are: 3, 4 and 5

constexpr bool GPSECHO = false; // Set to true to echo whatever the GPS sends on Serial

#define PMTK_SET_NMEA_OUTPUT_GGAONLY "$PMTK314,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*29" //This sentence doesn't appear in the official adafruit library but still gets accepted by the gps

#ifdef __AVR__
SSC_GPS_Client::SSC_GPS_Client(SoftwareSerial& serialPort): myGps(&serialPort), lastLongitudeValue(0), lastLatitudeValue(0) {} /**A double constructor is required as the feather m0 architecture doesn't accept a softwareserial */
#elif defined(ARDUINO_SAMD_FEATHER_M0_EXPRESS)
SSC_GPS_Client::SSC_GPS_Client(HardwareSerial& serialPort): myGps(&serialPort), lastLongitudeValue(0), lastLatitudeValue(0) {}
#endif
static Adafruit_GPS *gpsForInterrupt = NULL;

#ifdef ARDUINO_SAMD_FEATHER_M0_EXPRESS
#define CONCAT2(a, b) a ## b
#define CONCAT(a, b) CONCAT2(a, b)

static TC_Timer gpsTimer(CONCAT(TC_Timer::HwTimer::timerTC, GPS_CLIENT_TIMER));
#endif

#ifdef __AVR__ // interruption routine supported by __AVR__ architectures (arduino uno)
SIGNAL(TIMER0_COMPA_vect) {
  char c = gpsForInterrupt->read();
  // if you want to debug, this is a good time to do it!
  if (GPSECHO)
    if (c) UDR0 = c;
  // writing direct to UDR0 is much much faster than Serial.print
  // but only one character can be written at a time.
}
#elif defined(ARDUINO_SAMD_FEATHER_M0_EXPRESS) /** interruption routine supported by the feather m0 architecture*/
void CONCAT(TC, CONCAT(GPS_CLIENT_TIMER, _Handler))()
{
  if (gpsTimer.isYourInterrupt()) {
    char c = gpsForInterrupt->read();
    // if you want to debug, this is a good time to do it!
    if (GPSECHO) {
      if (c) Serial.print(c);
    }
    gpsTimer.clearInterrupt(); // clear the interrupt so that it will run again
  }
}
#else
#error "UNSUPPORTED BOARD"
#endif


void SSC_GPS_Client::begin(const Frequency updateFrequency)
{
  myGps.begin(9600); /**Sets an initialization baud rate for the gps. Tests show that it doesn't need to be changed in order to acquire data at 10HZ if using a GGAONLY NMEA sentence
                      WARNING: the baud rate sent to the gps, even if it doesn't accept it, will be conserved,
                      even if changing the code and televersing again, the only way to go back is to plug the gps out, losing the fix  */
  delay(1000); /**Gives time to the GPS to boot perfectly */
  gpsForInterrupt = &myGps;
  // Timer0 is already used for millis() - we'll just interrupt somewhere
  // in the middle and call the "Compare A" function above
#ifdef __AVR__
  OCR0A = 0xAF;
  TIMSK0 |= _BV(OCIE0A); //macro
#elif defined(ARDUINO_SAMD_FEATHER_M0_EXPRESS)
#else
#error "UNSUPPORTED BOARD" /**If any of the architectures supported by the class isn't used (__AVR__ or feather m0) */
#endif

  myGps.sendCommand(PMTK_SET_NMEA_OUTPUT_GGAONLY);
  /** This sets the GPS to output only GGA sentences:
    the default setting for the communication between the board and the GPS chip can accomodate at most 960 bytes of data if transmitting permanently.
    Taking into account a few different formatting characters and some flow interrupts, it is not safe to go more than 800 char/s.
    As a consequence:
     10 . RMC = 650 bytes => accepted
     10 . (RMC + GGA) = 1300 bytes => impossible
     10 . GGA = 650 bytes => working
  */

  changeFrequency(updateFrequency);
  /** Sets output rate of the GPS to desired frequency (passed in as parameter) */
}

void SSC_GPS_Client::changeFrequency(const Frequency updateFrequency) {
  switch (updateFrequency) {
    case Frequency::F1Hz :
      myGps.sendCommand(PMTK_SET_NMEA_UPDATE_1HZ);
      DPRINTLN(DBG_FREQUENCY, "GPS update frequency set to 1hz")
      break;
    case Frequency::F5Hz :
      myGps.sendCommand(PMTK_SET_NMEA_UPDATE_5HZ);
      DPRINTLN(DBG_FREQUENCY, "GPS update frequency set to 5hz")
      break;
    case Frequency::F10Hz :
      myGps.sendCommand(PMTK_SET_NMEA_UPDATE_10HZ);
      DPRINTLN(DBG_FREQUENCY, "GPS update frequency set to 10hz")
      break;
    default :
      DPRINTLN(DBG, "Unexpected frequency value. Terminating program.");
      DASSERT (false);
  }
#ifdef ARDUINO_SAMD_FEATHER_M0_EXPRESS
  gpsTimer.configure((int)updateFrequency * 100);
  gpsTimer.enable();
  DPRINTS(DBG_TIMER, "Timer frequency set to ")
  DPRINT(DBG_TIMER, ((int)updateFrequency * 100));
  DPRINTSLN(DBG_TIMER, " Hz");
#endif
}

void SSC_GPS_Client::readData(SSC_Record & record)
{
  record.newGPS_Measures = false;
  bool newReceived = myGps.newNMEAreceived();
  if (newReceived) {
    DPRINTLN(DBG_NMEA, myGps.lastNMEA());
    bool result = myGps.parse(myGps.lastNMEA());
    if (!result) {
      DPRINTSLN(DBG_DIAGNOSTIC, "ERROR PARSING NMEA");
    }
  }

  // Valid fix are 1 (GPS fix), 2 (DGPS fix), 3 (PPS fix)
  bool fixOK = ((myGps.fixquality == 1) || (myGps.fixquality == 2)  || (myGps.fixquality == 3));
  if (fixOK && newReceived) {
    DPRINTSLN(DBG_NMEA, "fix ok in in NMEA");
    record.newGPS_Measures = true;
    record.GPS_LongitudeDegrees = myGps.longitudeDegrees;
    record.GPS_LatitudeDegrees = myGps.latitudeDegrees;
    lastLongitudeValue = myGps.longitudeDegrees;
    lastLatitudeValue = myGps.latitudeDegrees;
#ifdef INCLUDE_GPS_VELOCITY
    record.GPS_VelocityKnots = myGps.speed;
    record.GPS_VelocityAngleDegrees = myGps.angle;
#endif
    record.GPS_Altitude = myGps.altitude;
  }
  else {
    //DPRINTSLN(DBG_NMEA, "fix not ok in in NMEA or no new data");
    record.GPS_LongitudeDegrees = lastLongitudeValue;
    record.GPS_LatitudeDegrees = lastLatitudeValue;
  }
}
