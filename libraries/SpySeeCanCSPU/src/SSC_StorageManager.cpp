/*
   SSC_StorageManager.cpp
*/

#include "SSC_StorageManager.h"
#include "StringStream.h"

SSC_StorageManager::SSC_StorageManager(unsigned int CampainDuration, unsigned int AcquisitionPeriod)
  : StorageManager(sizeof(SSC_Record), CampainDuration, AcquisitionPeriod) {
}

void SSC_StorageManager::storeSSC_Record(const SSC_Record& record, const bool useEEPROM) {
  DPRINTSLN(DBG_STORAGE, "SSC_StorageManager::storeOneRecord");
  String data;
  data.reserve(200);
  StringStream sstr(data);
  record.printCSV(sstr);
  const byte * binaryData = (const byte*) &record;
  storeOneRecord(binaryData, data, useEEPROM);
}
