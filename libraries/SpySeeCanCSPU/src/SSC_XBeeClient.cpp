/*
    SSC_XBeeClient.cpp
*/

#include "SSC_XBeeClient.h"
#define DEBUG
#include "DebugCSPU.h"
#define DBG_DIAGNOSTIC 1
#define DBG_STREAM 0
#define DBG_SEND 0


bool SSC_XBeeClient::send(const SSC_Record& record, int timeOutCheckResponse) {
  // Beware of byte alignment. Only even adresses can be adressed directly
  // in SAMD 16-bit architecture.
  DPRINTSLN(DBG_SEND, "SSC_XBeeClient::send(record) called");

  uint8_t buffer[sizeof(SSC_Record) + 2];
  buffer[0] = (uint8_t) SSC_RecordType::DataRecord;
  buffer[1] = 0;
  memcpy(buffer + 2, &record, sizeof(record));

#ifdef XBEECLIENT_INCLUDES_UTILITIES
#ifdef DBG_PRINT_OUTGOING_FRAMES
  if (displayOutgoingFramesOnSerial) {
    displayFrame((uint8_t *)  buffer, (uint8_t) (sizeof(record) + 2));
  }
#endif
#endif
  return XBeeClient::send((uint8_t *)  buffer, (uint8_t) (sizeof(record) + 2), timeOutCheckResponse);
}

bool SSC_XBeeClient::getDataRecord(SSC_Record& record, const uint8_t* data, uint8_t dataSize) {
  if (dataSize != (sizeof(SSC_Record) + 2)) {
    DPRINTS(DBG_DIAGNOSTIC, "*** Error: inconsistent size. size=");
    DPRINT(DBG_DIAGNOSTIC, dataSize);
    DPRINTS(DBG_DIAGNOSTIC, ", SSC_Record size=");
    DPRINTLN(DBG_DIAGNOSTIC, sizeof(SSC_Record));
    return false;
  }
  if (data[0] != (uint8_t) SSC_RecordType::DataRecord) {
    DPRINTS(DBG_DIAGNOSTIC, "*** Error: inconsistent record type (");
    DPRINT(DBG_DIAGNOSTIC, data[0]);
    DPRINTS(DBG_DIAGNOSTIC, ", expected ");
    DPRINTLN(DBG_DIAGNOSTIC, (uint8_t) SSC_RecordType::DataRecord);
    return false;
  }
  memcpy(&record, data + 2, sizeof(record));
  return true;
}

#ifdef XBEECLIENT_INCLUDES_UTILITIES

void SSC_XBeeClient::displayFrame(uint8_t* data, uint8_t dataSize)
{
  unsigned char type = data[0];
  SSC_Record rec;
  const char* s;
  Serial << "--- Frame content ---" << ENDL;
  switch (type) {
    case (int) SSC_RecordType:
      Serial << "DataRecord message" << ENDL;
      if (getDataRecord(rec, data, dataSize)) {
        rec.print(Serial);
        Serial << ENDL << "  --- End of record ---" << ENDL;
      }
      break;
    default:
      Serial << "*** Unsupported type: " << type << ENDL;
  }
  Serial << "--- End of frame content ---" << ENDL;
}

bool SSC_XBeeClient::send(  uint8_t* data,
                            uint8_t dataSize,
                            int timeOutCheckResponse,
                            uint32_t SH, uint32_t SL) {
  DPRINTSLN(DBG_STREAM, "SSC_XBeeClient::send() ");
  if (displayOutgoingFramesOnSerial) {
    displayFrame(data, dataSize);
  }
  return XBeeClient::send(data, dataSize, timeOutCheckResponse, SH, SL);
};
#endif
 bool SSC_XBeeClient::receiveRecord(SSC_Record& incomingRecord)
 {
      bool result = false;
      uint8_t* payloadPtr;
      uint8_t payloadSize;
      if (receive(payloadPtr, payloadSize)) {
        if (payloadSize > 0)  {
          if (getDataRecord(incomingRecord, payloadPtr, payloadSize))  {
            result = true;
          } else {
            DPRINTS(DBG_DIAGNOSTIC, "*** Error: inconsistent SSC record received");
          }
        } else {
          DPRINTS(DBG_DIAGNOSTIC, "*** Null size payload received");
        }
      } // received.
      return result;
    }
