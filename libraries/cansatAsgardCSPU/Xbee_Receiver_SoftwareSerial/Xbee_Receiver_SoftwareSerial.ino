/**
   This is a small sketch which will be used to check rapidly
*/
const int RF_rxPinOnUNO = 9;
const int RF_TxPinOnUno = 11;
char incomingByte;

#ifdef ARDUINO_SAMD_FEATHER_M0_EXPRESS
HardwareSerial &RF = Serial1;
#else
#  include "SoftwareSerial.h"
SoftwareSerial RF(RF_RxPinOnUno, RF_TxPinOnUno);
#endif

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  Serial.begin(115200);
  RF.begin(115200);
  while(!Serial);
  Serial.println("Starting XBee Communication");
}

void loop() {

  if (RF.available() > 0) {
    incomingByte = RF.read();
    Serial.print("received : ");
    Serial.println (incomingByte);
  }

  if (Serial.available() > 0) {
    incomingByte = Serial.read();
    Serial.print(incomingByte);
  }

}

