/*
 * Serial3.h
 * 
 * This file must be included to define the Serial3 object when it is not defined in the core
 * for a particular board (If Serial3 is defined, including this file will not modify it.
 * 
 * Currently supports only boards with ATSAMD21G18 controller 
 */

#if defined(UBRR3H) || defined(HAVE_HWSERIAL3)
    // If any of the above is defined, Serial3 global object is available, 
    // Nothing need to be done.
#elif defined(__SAMD21G18A__)
#  ifdef ARDUINO_SAMD_FEATHER_M0_EXPRESS
#    error "Serial.h cannot be used on Feather M0 Express: Sercom2 is used for Flash memory"
#  endif 
#  include <Arduino.h>
 
/** @ingroup cansatAsgardCSPU
 *  @brief This class encapsulates the configuration and use of Sercom module 2 to manage a second 
 *  serial port on µControllers with Sercom (Serial communication) modules. It is 
 *  developped for the ATSAMD21G18 controller used by Adafruit FeatherM0 Express board (although it 
 *  should work for any board based on the same controller.
 *  TX is on D4 and RX on D3.
 *  NB: Serial3 is a subclass of Uart, HardwareSerial and Stream. 
 *  
 *  @usage
 *  @code
 *  #include "Serial3.h"
 *  
 *  //... in your code ...
 *  Serial3.begin(115200);
 *  SerialZ.println("xxxx");
 *  if (Serial3.available()) {
 *    char c = Serial3.read();
 *    // ....
 *  }
 *  @endcode
 *  Important notes:
 *   - Some Sercoms are configured by default and should usually not be modified: 
 *     The µC ATSAMD21G18 has 5 sercom modueles: 0 is reserved 
 *     for the USB serial, 3 is used for the I2C bus, 4 for the SPI bus,  
 *     5 for Programming/Debug port (if any, not on FeatherM0). 
 *   - Not all sercoms can be configured on all pins, due to limitations in the 
 *     multiplexers (or in the configuration files). This class supports the use
 *     of Sercom 1, with TX on D4, RX on D3. 
 *   - A similar class exists for Sercom 1, with TX on D10, RX on D11
 *  Reference: @link https://learn.adafruit.com/using-atsamd21-sercom-to-add-more-spi-i2c-serial-ports/creating-a-new-serial
 *  
 */
class SercomSerial2 : public Uart {
 public:
    SercomSerial2();
    
    /** Obtain the TX pin number */
    byte getTX() { return tx;};
    /** Obtain the RX pin number */
    byte getRX() { return rx;};
 private:
    byte rx;
    byte tx;
 };

extern SercomSerial2 Serial3;

#else
#error "Serial3.h: currently only supports board with a second UART or using µC SAMD21G18A"
#endif
