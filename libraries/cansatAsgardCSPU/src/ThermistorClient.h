/*
   ThermistorClient.h
*/
#pragma once

#include "Arduino.h"

class ThermistorClient {
  public:
    ThermistorClient(const uint8_t theAnalogPinNbr, const float theRefResistor, const float theA, const float theB, const float theC, const float theD, const float theSerialResistor);
    float readTemperature()const;
  private:
    float readVoltage()const;
    float readResistance()const;
    float A, B, C, D;
    float analogPinNbr;
    float refResistor, serialResistor;
};
