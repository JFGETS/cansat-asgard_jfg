/**
   XBeeClient.cpp
   Started on 21 April 2019
*/
#include "XBeeClient.h"
#define DEBUG
#include "DebugCSPU.h"
#include <XBee.h>

#define DBG_SEND 0
#define DBG_RECEIVE 0 
#define DBG_STREAM 0
#define DBG_DIAGNOSTIC 1

XBeeClient::XBeeClient(uint32_t SH, uint32_t SL) :
  xbee(),
  defaultReceiverAddress(SH, SL),
  msgBuffer(),
  sstr(msgBuffer)
{
  msgBuffer.reserve(200);
}

void XBeeClient::begin(HardwareSerial &stream) {
  xbee.begin(stream);
}

bool XBeeClient::send(	uint8_t* data,
                        uint8_t dataSize,
                        int timeOutCheckResponse,
                        uint32_t SH, uint32_t SL) {

  if (dataSize > MAX_FRAME_DATA_SIZE - 12) {
    DPRINTS(DBG_DIAGNOSTIC, "XBeeClient::send: payload too large: ");
    DPRINT(DBG_DIAGNOSTIC, dataSize);
    DPRINTSLN(DBG_DIAGNOSTIC, " bytes");
    return false;
  }
  XBeeAddress64 receiverAddress(SH, SL);
  if ((SH == 0L) && (SL == 0L)) {
    receiverAddress = defaultReceiverAddress;
  }

  // Should we give values back ? Like the SD_Logger for instance to know exactly the type of ***ERROR*** it is
  bool result = false;
  ZBTxRequest zbTx = ZBTxRequest(receiverAddress, data, dataSize); //Creating an object to send the data
  ZBTxStatusResponse txStatus = ZBTxStatusResponse();
  xbee.send(zbTx);
  if (timeOutCheckResponse == 0) {
    // If time is null we do not check for reception.
    return true;
  }
  if (xbee.readPacket(timeOutCheckResponse)) { //Wait upto 500 ms a response from the other Xbee that a packet was received - Change value to variable given by user
    if (xbee.getResponse().getApiId() == ZB_TX_STATUS_RESPONSE) {
      DPRINTS(DBG_SEND, "We got the ZB_TX_STATUS_RESPONSE...");
      xbee.getResponse().getZBTxStatusResponse(txStatus);
      // get the delivery status, the fifth byte

      uint8_t status = txStatus.getDeliveryStatus();
      DPRINTS(DBG_SEND, "Status as delivered = ");
      DPRINTLN(DBG_SEND, status);

      if (txStatus.getDeliveryStatus() == SUCCESS) {
        DPRINTSLN(DBG_SEND, "SUCCESS");
        result = true;
      } else {
        // ***ERROR***
        DPRINTSLN(DBG_DIAGNOSTIC, "***ERROR*** The other XBee didn't receive our packet");
      }
    }
  } else if (xbee.getResponse().isError()) {
    // ***ERROR*** we had no response
    DPRINTSLN(DBG_DIAGNOSTIC, "***ERROR*** reading packet. ErrCode=");
    DPRINTLN(DBG_SEND, xbee.getResponse().getErrorCode());
    result = DBG_DIAGNOSTIC;
  } else {
    // ***ERROR***
    //Revise the message
    DPRINTSLN(DBG_DIAGNOSTIC, "***ERROR*** - No timely TX Status Response");
  }
  return result;
}

bool XBeeClient::receive(uint8_t* &payload, uint8_t& payloadSize) {                                 //POURQUOI ????
  bool result = false;
  ZBRxResponse rx = ZBRxResponse();
  ModemStatusResponse msr = ModemStatusResponse();
  xbee.readPacket(); //Checks the communication at every loop
  if (xbee.getResponse().isAvailable()) {
    DPRINTS(DBG_RECEIVE, "We got something: ");
    uint8_t apiId = xbee.getResponse().getApiId();
    if (apiId == ZB_RX_RESPONSE) {
      DPRINTSLN(DBG_RECEIVE, "a zb rx packet!");
      //Now fill our zb rx class
      xbee.getResponse().getZBRxResponse(rx);
      uint8_t resp = rx.getOption();
      if (resp == ZB_PACKET_ACKNOWLEDGED) {
        // the sender got an ACK
        DPRINTSLN(DBG_RECEIVE, "Sender got en ACK - good");

        payload = rx.getData();
        payloadSize = rx.getDataLength();
        result = true;
      } else {
        //***ERROR***
        DPRINTSLN(DBG_DIAGNOSTIC, "***ERROR*** Received packet but sender didn't get an ACK. API ID=");
        DPRINTLN(DBG_DIAGNOSTIC, apiId);
        DPRINTS(DBG_DIAGNOSTIC, ", ZBRxResponse: ");
        DPRINTLN(DBG_DIAGNOSTIC, resp);
      }
    } else if (apiId == MODEM_STATUS_RESPONSE) {
      xbee.getResponse().getModemStatusResponse(msr);
      // the local XBee sends this response on certain events, like association/dissociation
      DPRINTS(DBG_RECEIVE, "A certain event happened association/dissociation: ");
      auto msrStatus = msr.getStatus() ;
      if (msrStatus == ASSOCIATED) {
        DPRINTSLN(DBG_RECEIVE, "It's an association - Yeay this is great!");
        result = true;
      } else if (msrStatus == DISASSOCIATED) {
        //***ERROR***
        DPRINTSLN(DBG_DIAGNOSTIC, "*** Error MSR status: dissociation");
      } else {
        //***ERROR***
        DPRINTS(DBG_DIAGNOSTIC, "*** Error MSR status unknown: ");
        DPRINTLN(DBG_DIAGNOSTIC, msrStatus);
      }
    } else {
      /*  Not error
            DPRINTS(DBG_DIAGNOSTIC, "*** Error unexpected API ID = ");
            DPRINTLN(DBG_DIAGNOSTIC, apiId);
            result = false;
      */
    }
  } else if (xbee.getResponse().isError()) {
    //***ERROR***
    DPRINTS(DBG_DIAGNOSTIC, "*** ERROR in reading packet. Err code=");
    DPRINTLN(DBG_DIAGNOSTIC, xbee.getResponse().getErrorCode());
    switch (xbee.getResponse().getErrorCode()) {
      case CHECKSUM_FAILURE:
        DPRINTLN(DBG_DIAGNOSTIC, "Checksum failure");
        break;
      case PACKET_EXCEEDS_BYTE_ARRAY_LENGTH:
        DPRINTS(DBG_DIAGNOSTIC, "Packet exceeds byte-array length. Should not exceed (bytes)  ");
        DPRINTLN(DBG_DIAGNOSTIC, MAX_FRAME_DATA_SIZE - 10);
        break;
      case UNEXPECTED_START_BYTE:
        DPRINTLN(DBG_DIAGNOSTIC, "Unexpected start byte");
        break;
      default:
        DPRINTLN(DBG_DIAGNOSTIC, "Unknown error code.");
    }
  }
  return result;
}

void XBeeClient::openStringMessage(uint8_t type) {
  msgBuffer = "  ";		// Reserve 2 bytes.
  msgBuffer[0] = type; // Store value of byte, not string representation.
  msgBuffer[1] = 0xff; // Skip byte to keep even alignment of the string.
  DPRINTS(DBG_STREAM,  "XBeeClient::openStringMessage. buffer='");
  DPRINT(DBG_STREAM,  (msgBuffer.c_str() + 2));
  DPRINTS(DBG_STREAM,  "', type=");
  DPRINTLN(DBG_STREAM, (uint8_t) msgBuffer[0]);
}

bool XBeeClient::closeStringMessage() {
  DPRINTS(DBG_STREAM,  "XBeeClient::closeStringMessage. buffer='");
  DPRINT(DBG_STREAM,  (msgBuffer.c_str() + 2));
  DPRINTS(DBG_STREAM,  "', type=");
  DPRINTLN(DBG_STREAM, (uint8_t) msgBuffer[0]);

  auto sLength = msgBuffer.length();
  if (sLength == 0) {
    DPRINTSLN(DBG_DIAGNOSTIC, "Error: closing empty message (no type!)");
    return false;
  }
  if (sLength == 2) {
    DPRINTSLN(DBG_DIAGNOSTIC, "Error: closing empty message (only type)");
    return false;
  }

  // Also send the terminating '\0'.
  return send((uint8_t *) msgBuffer.c_str(), (uint8_t) msgBuffer.length() + 1);
}
