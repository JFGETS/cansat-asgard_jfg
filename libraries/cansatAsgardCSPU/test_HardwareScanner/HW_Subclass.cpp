/*
   HW_Subclass.cpp
*/

#include "HW_Subclass.h"
#define DEBUG
#define USE_ASSERTIONS
#include "DebugCSPU.h"
#include "Arduino.h"

HW_Subclass::HW_Subclass() : HardwareScanner() {
  DPRINTSLN(1, "HW_Subclass constructor called");
}

byte HW_Subclass::getExternalEEPROM_Address(const byte EEPROM_Number) const {
  switch (EEPROM_Number) {
    case 1:
      return 17;
      break;
    case 2:
      return 18;
      break;
    case 3:
      return 19;
      break;
    default:
      DASSERT(false);
  }
  return 0;
}

unsigned int HW_Subclass::getExternalEEPROM_LastAddress(const byte EEPROM_Number) const {
  switch (EEPROM_Number) {
    case 0:
      return (unsigned int)(32L * 1024L -1L) ;
      break;
    case 1:
      return 16 * 1024-1;
      break;
    case 2:
      return 8 * 1024 -1;
      break;
    default:
      DASSERT(false);
  }
  return 0;
}

void HW_Subclass::checkAllSPI_Devices() {
  DPRINTSLN(1, "HW_Subclass::checkAllSPI_Devices");
}

byte HW_Subclass::getPinNbr_StorageLED(){
	 return 2;
}
byte HW_Subclass::getPinNbr_AcquisitionLED(){
	 return 5;
}
