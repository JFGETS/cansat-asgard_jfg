int Nsteps=1024;
float Vmax=3.3;

#include "arduino.h"
void setup(){
  Serial.begin(115200);
  // analogReadResolution(8);
  
}

void loop(){
  Serial.println(analogRead(A0));
  float V = analogRead(A0) * (Vmax / (Nsteps - 1));
  Serial.println(V);
}
