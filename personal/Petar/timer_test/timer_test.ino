#define USE_TIMER
#define DEBUG

#include "DebugCSPU.h"
#include "Timer.h"


void test(){
  DBG_TIMER("function test");
  delay(1000);
}

void setup() {
  DINIT(115200);
  delay(5000);
  Serial << "End delay" << ENDL;
  test();

}

void loop() {
  // put your main code here, to run repeatedly:

}
