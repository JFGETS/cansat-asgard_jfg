/*
 * Small test to compare gyro readings by LSM and NXP sensors. 
 */
 
#define DEBUG
#include "DebugCSPU.h"

#include <Adafruit_Sensor.h>
#include <Adafruit_LSM9DS0.h>
#include <Adafruit_FXAS21002C.h>

Adafruit_FXAS21002C gyro = Adafruit_FXAS21002C(0x0021002C);
Adafruit_LSM9DS0 lsm = Adafruit_LSM9DS0(1000);

void setup() {
  DINIT(115200);
  if (!gyro.begin())
  {
    /* There was a problem detecting the gyro ... check your connections */
    Serial.println("Ooops, no gyro detected ... Check your wiring!");
    while (1);
  }
  if (!lsm.begin())
  {
    /* There was a problem detecting the L3GD20 ... check your connections */
    Serial.println("Ooops, no LSM9DS0 detected ... Check your wiring!");
    while (1);
  }
  lsm.setupGyro(lsm.LSM9DS0_GYROSCALE_245DPS);
  Serial << "Setup OK" << ENDL;
}

void loop() {
  sensors_event_t gyroNXP_Event, gyroLSM_Event;

  gyro.getEvent(&gyroNXP_Event);
  lsm.getEvent(NULL, NULL, &gyroLSM_Event, NULL);
  // LSM gyro is in dps, NXP gyro is in rps.

  Serial << "NXP raw: " << gyro.raw.x << '/' << gyro.raw.y << '/' << gyro.raw.z << ENDL;  
  Serial << "NXP dps calculé ici: " << gyro.raw.x*GYRO_SENSITIVITY_250DPS << '/' << gyro.raw.y*GYRO_SENSITIVITY_250DPS << '/' << gyro.raw.z*GYRO_SENSITIVITY_250DPS << ENDL;  
  Serial << "NXP dps: " << gyroNXP_Event.gyro.x*180.0/PI << '/' << gyroNXP_Event.gyro.y*180.0/PI << '/' << gyroNXP_Event.gyro.z*180.0/PI << ENDL;
  Serial << "LSM dps: " << gyroLSM_Event.gyro.x << '/' << gyroLSM_Event.gyro.y << '/' << gyroLSM_Event.gyro.z << ENDL;
  Serial << "LSM/NXP: " << gyroLSM_Event.gyro.x/(gyroNXP_Event.gyro.x*180.0/PI) << '/' 
         << gyroLSM_Event.gyro.y/(gyroNXP_Event.gyro.y*180.0/PI) << '/' 
         << gyroLSM_Event.gyro.z/(gyroNXP_Event.gyro.z*180.0/PI) << ENDL;
  Serial << "---------------------" << ENDL;

  delay(1000);
}
