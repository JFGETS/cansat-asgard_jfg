
#include "EEPROM_Bank.h"

#define USE_ASSERTIONS
#include "DebugCSPU.h"

#include "IsatisConfig.h"
#include "IsatisHW_Scanner.h"
#include "IsatisDataRecord.h"


IsatisDataRecord buffer;

void setup() {
  DINIT(9600);
  IsatisHW_Scanner hw;
  hw.init(1,127);
  hw.printFullDiagnostic(Serial);

  EEPROM_Bank bank(1);
  bool result= bank.init(EEPROM_KeyValue, hw, sizeof(IsatisDataRecord));
  if (!result) {
     Serial << F("Error during EEPROM_Bank init") << ENDL;
  }

  unsigned long numRecords = bank.recordsLeftToRead();
  unsigned long numFreeRecords = bank.getNumFreeRecords();
  
  Serial << F("EEPROMS contain ") << numRecords << F(" Isatis records") << ENDL;
  Serial << F("        has room for another ") << numFreeRecords <<F(" Isatis records") << ENDL;
  float recordPerSecond = 1000.0/IsatisAcquisitionPeriod;
  Serial << F("        (i.e. ") << numFreeRecords/recordPerSecond /60.0 << F(" minutes of operation)") << ENDL;
  buffer.printCSV_Header(Serial);  
  Serial << ENDL;
  for (unsigned long i = 0; i < numRecords; i++) 
  {
    bank.readOneRecord((byte *) &buffer, sizeof(buffer));
    buffer.printCSV(Serial);
    Serial<<ENDL;
  }

  Serial << ENDL;
  Serial << F("End of job") << ENDL;
}

void loop() {
  // put your main code here, to run repeatedly:

}
