/*
   A class implementing a blinking LED
   Usage:
      BlinkingLED myLed(13);

      loop() {
        myLed.run();
        // ... any other stuff
      }
*/

class BlinkingLed {
  public:
    BlinkingLed(const byte thePinNumber, const unsigned int thePeriod) : pinNumber(thePinNumber), period(thePeriod) {};

    void run() {
      if ((millis() - timestamp) >= period) {
        // Time to toggle
        Serial.print("Toggle led. milli=");
        Serial.println(millis());
        if (currentlyOn) {
          digitalWrite(pinNumber, LOW);
        }
        else {
          digitalWrite(pinNumber, HIGH);
        }
        currentlyOn = !currentlyOn;
        timestamp = millis();
      }
    };
  private:
    bool currentlyOn;
    byte pinNumber;
    unsigned int period;
    unsigned long timestamp;
};
